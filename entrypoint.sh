#!/bin/sh

set -xeuo pipefail

MMPROXY_MARK="${MMPROXY_MARK:-123}"

init() {
  set +e
  echo 1 > /proc/sys/net/ipv4/conf/eth0/route_localnet
  ip rule add from 127.0.0.1/8 iif lo table "${MMPROXY_MARK}"
  ip route add local 0.0.0.0/0 dev lo table "${MMPROXY_MARK}"
  ip -6 rule add from ::1/128 iif lo table "${MMPROXY_MARK}"
  ip -6 route add local ::/0 dev lo table "${MMPROXY_MARK}"
  exit 0 # this is a hack so the init doesn't fail
}

run() {
  MMPROXY_ALLOWED_NETWORKS="${MMPROXY_ALLOWED_NETWORKS:-"0.0.0.0/0 ::/0"}"

  MMPROXY_DEST_PORT="${MMPROXY_DEST_PORT}"
  MMPROXY_DEST_PORT_4="${MMPROXY_DEST_PORT_4:-"${MMPROXY_DEST_PORT}"}"
  MMPROXY_DEST_PORT_6="${MMPROXY_DEST_PORT_6:-"${MMPROXY_DEST_PORT}"}"

  MMPROXY_DEST_IP_4="${MMPROXY_DEST_IP_4:-"127.0.0.1"}"
  MMPROXY_DEST_IP_6="${MMPROXY_DEST_IP_6:-"[::1]"}"

  for net in ${MMPROXY_ALLOWED_NETWORKS}; do
    echo "${net}" >> /networks.txt
  done

  exec /go-mmproxy \
    -mark "${MMPROXY_MARK}" \
    -allowed-subnets /networks.txt \
    -l "0.0.0.0:${MMPROXY_LISTEN_PORT}" \
    -4 "${MMPROXY_DEST_IP_4}:${MMPROXY_DEST_PORT_4}" \
    -6 "${MMPROXY_DEST_IP_6}:${MMPROXY_DEST_PORT_6}" \
    "$@"
}

CMD="${1}"
shift

if [ "$CMD" = "go-mmproxy" ]; then
  run "$@"
elif [ "$CMD" = "init" ]; then
  init
else
  exec /bin/sh -c "$CMD $*"
fi
