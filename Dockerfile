FROM golang:1.19-alpine as builder

ARG GOMMPROXY_REF=2.1

RUN apk add --no-cache git

RUN git clone --depth 1 --branch $GOMMPROXY_REF https://github.com/path-network/go-mmproxy.git /mmproxy

WORKDIR /mmproxy

RUN go mod download
RUN go build -o go-mmproxy .

FROM alpine:3.17

WORKDIR /

RUN apk add --no-cache iptables iproute2

COPY --from=builder /mmproxy/go-mmproxy /go-mmproxy

COPY ./entrypoint.sh /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]
